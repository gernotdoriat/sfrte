let sfGrid = new ej.grids.Grid({
    allowCsvExport: true,
    allowExcelExport: true,
    allowPdfExport: true,
    dataSource: [{ A: 1, B: 2, C: 3 }],
    selectionSettings: { mode: "Cell", type: "Single" },
    editSettings: { allowEditing: false },

    // alle columns initial unsichtbar
    columns: [{ field: "A" }, { field: "B" }, { field: "C" }],
    toolbar: ["ExcelExport", "PdfExport", "CsvExport", "ColumnChooser"],
})
sfGrid.appendTo("#div-sfgrid")

let sfRTE = new ej.richtexteditor.RichTextEditor({
    value: "<b> Transformationskleid #1 </b> <br> Material: Modal <br> Grössen: S - XL <br> Farben: Schwarz, Grau, Senfgelb <br> Preis: 229,- <br> <br> <b> Kokon Mütze </b> <br> Material: Baumwolle <br> Farben: Schwarz, Grau, Senfgelb <br> Preis: 29,-",
})
sfRTE.appendTo("#div-rte")
